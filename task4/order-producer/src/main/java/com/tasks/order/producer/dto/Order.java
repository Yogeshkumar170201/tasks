package com.tasks.order.producer.dto;

import lombok.*;


/**
 * Represent Order
 */
@Data
@Getter
@Setter
@ToString
@Builder
public class Order {
    private String name;
    private Long timeOfPreparation;
    private String type;
}
