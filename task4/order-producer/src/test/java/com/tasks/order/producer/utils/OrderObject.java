package com.tasks.order.producer.utils;

import com.tasks.order.producer.dto.Order;

/**
 * Dummy Objects of Order class for testing
 */
public class OrderObject {
    public static final Order VALID_ORDER = Order.builder()
            .name("Pizza")
            .timeOfPreparation(30L)
            .type("Veg")
            .build();

    public static final Order NULL_ORDER = null;

    public static final Order NULL_NAME_ORDER = Order.builder()
            .timeOfPreparation(30L)
            .type("Veg")
            .build();

    public static final Order EMPTY_NAME_ORDER = Order.builder()
            .name("")
            .timeOfPreparation(30L)
            .type("Veg")
            .build();

    public static final Order NON_ALPHABETIC_NAME_ORDER = Order.builder()
            .name("322rcd")
            .timeOfPreparation(30L)
            .type("Veg")
            .build();

    public static final Order TIME_OF_PREPARATION_LESS_THAN_EQUALS_TO_ZERO_ORDER = Order.builder()
            .name("Pizza")
            .timeOfPreparation(-1L)
            .type("Veg")
            .build();

    public static final Order NULL_TYPE_ORDER = Order.builder()
            .name("Pizza")
            .timeOfPreparation(10L)
            .build();

    public static final Order EMPTY_TYPE_ORDER = Order.builder()
            .name("Pizza")
            .timeOfPreparation(10L)
            .type("")
            .build();

    public static final Order NON_ALPHABETIC_TYPE_ORDER = Order.builder()
            .name("Pizza")
            .timeOfPreparation(10L)
            .type("234jiik")
            .build();
}
