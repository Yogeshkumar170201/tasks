package com.tasks.order.producer.utils;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Dummy Objects of Responses for testing
 */
public class ResponseObject {
    private static Map<String, String> res = new HashMap<>();
    public static Map<String, String> getOrderProducerController_OrderProducer_SuccessResponse(){
        res.put("message", "Order Produced Successfully");
        res.put("HttpStatus", String.valueOf(HttpStatus.CREATED));
        return res;
    }

    public static Map<String, String> getOrderProducerController_OrderProducer_FailureResponse(){
        res.put("message", "Inavlid Order");
        res.put("HttpStatus", String.valueOf(HttpStatus.BAD_REQUEST));
        return res;
    }

}
