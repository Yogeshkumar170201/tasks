package com.tasks.order.producer.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.tasks.order.producer.utils.OrderObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.mockito.Mockito.*;


/**
 * OrderSerializer Unit Testcase
 */
@ExtendWith(MockitoExtension.class)
class OrderSerializerTest {

    @Mock
    private JsonGenerator jsonGenerator;

    @Mock
    private SerializerProvider serializerProvider;

    @Mock
    private ObjectMapper objectMapper;

    @Spy
    private OrderSerializer orderSerializer;

    /**
     * @throws IOException when jsonGenerator unable to write object
     */
    @Test
    void serialize_ShouldWriteOrderObjectToJsonGenerator() {

        orderSerializer.serialize(OrderObject.VALID_ORDER, jsonGenerator, serializerProvider);

        try {
            verify(jsonGenerator).writeObject(OrderObject.VALID_ORDER);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}