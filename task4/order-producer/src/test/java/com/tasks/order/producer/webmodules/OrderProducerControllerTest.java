package com.tasks.order.producer.webmodules;

import com.tasks.order.producer.dto.Order;
import com.tasks.order.producer.exception.InvalidOrderException;
import com.tasks.order.producer.service.OrderProducerService;
import com.tasks.order.producer.utils.OrderObject;
import com.tasks.order.producer.utils.ResponseObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Controller class unit testcases
 */
@ExtendWith(MockitoExtension.class)
class OrderProducerControllerTest {

    @Mock
    private OrderProducerService orderProducerService;

    @InjectMocks
    private OrderProducerController orderProducerController;

    /**
     * unit testcase of orderProduce in controller layer if order is valid
     */
    @Test
    public void OrderProducerController_OrderProduce_Success() throws InvalidOrderException {

        doNothing().when(orderProducerService).produceOrder(ArgumentMatchers.any(Order.class));

        ResponseEntity<Map<String, String>> actual =  orderProducerController.orderProducer(OrderObject.VALID_ORDER);


        verify(orderProducerService).produceOrder(ArgumentMatchers.eq(OrderObject.VALID_ORDER));


        assertThat(actual.equals(ResponseObject.getOrderProducerController_OrderProducer_SuccessResponse()));
    }

    /**
     * unit testcase of orderProduce in controller layer if order is invalid
     */
    @Test
    public void OrderProducerController_OrderProduce_Failure() throws InvalidOrderException {

        doThrow(new InvalidOrderException("Invalid Order", HttpStatus.BAD_REQUEST)).when(orderProducerService).produceOrder(OrderObject.NULL_ORDER);

        ResponseEntity<Map<String, String>> actual =  orderProducerController.orderProducer(OrderObject.NULL_ORDER);


        verify(orderProducerService).produceOrder(OrderObject.NULL_ORDER);


        assertThat(actual.equals(ResponseObject.getOrderProducerController_OrderProducer_FailureResponse()));
    }
}