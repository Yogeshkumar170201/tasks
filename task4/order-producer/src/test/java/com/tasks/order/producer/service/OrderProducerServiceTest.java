package com.tasks.order.producer.service;

import com.tasks.order.producer.constants.AppConstants;
import com.tasks.order.producer.dto.Order;
import com.tasks.order.producer.exception.InvalidOrderException;
import com.tasks.order.producer.utils.OrderObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

/**
 * Order Producer Service Test
 */
@ExtendWith(MockitoExtension.class)
class OrderProducerServiceTest {

    @InjectMocks
    private OrderProducerService orderProducerService;

    @Mock
    private KafkaTemplate<String, Order> kafkaTemplate;


    /**
     * Valid Order produces order
     * @return null
     */
    @Test
    public void OrderProducerService_ProduceOrder_WithValidOrder_ReturnsNull() throws InvalidOrderException {

        when(kafkaTemplate.send(any(String.class), any(Order.class))).thenReturn(null);
        orderProducerService.produceOrder(OrderObject.VALID_ORDER);
        verify(kafkaTemplate, times(1)).send(AppConstants.TOPIC_NAME, OrderObject.VALID_ORDER);
    }

    /**
     * @throws InvalidOrderException when order is null
     */
    @Test
    public void OrderProducerService_ProduceOrder_WithNullOrder_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.NULL_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Invalid Order");
        }
   }

    /**
     * @throws InvalidOrderException when order name is null
     */
   @Test
   public void OrderProducerService_ProduceOrder_WithNullName_ThrowsInvalidOrderException(){
       try {
           orderProducerService.produceOrder(OrderObject.NULL_NAME_ORDER);
       }catch (InvalidOrderException e) {
           assertThat(e.getErrorMessage()).isEqualTo("Name of order is not valid");
       }
   }

    /**
     * @throws InvalidOrderException when order name is empty
     */
    @Test
    public void OrderProducerService_ProduceOrder_WithEmptyName_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.EMPTY_NAME_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Name of order is not valid");
        }
    }

    /**
     * @throws InvalidOrderException when order name contains non-alphabetic characters
     */
    @Test
    public void OrderProducerService_ProduceOrder_WithNonAlphabeticName_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.NON_ALPHABETIC_NAME_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Name of order is not valid");
        }
    }

    /**
     * @throws InvalidOrderException when timeOfPreparation is less than equals to zero
     */
    @Test
    public void OrderProducerService_ProduceOrder_WithTimeOfPreparationIsLessThanEqualsToZero_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.TIME_OF_PREPARATION_LESS_THAN_EQUALS_TO_ZERO_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Time of preparation should be positive");
        }
    }

    /**
     * @throws InvalidOrderException when order type is null
     */
    @Test
    public void OrderProducerService_ProduceOrder_NullType_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.NULL_TYPE_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Type of order is not valid");
        }
    }

    /**
     * @throws InvalidOrderException when order type is empty
     */
    @Test
    public void OrderProducerService_ProduceOrder_EmptyType_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.EMPTY_TYPE_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Type of order is not valid");
        }
    }

    /**
     * @throws InvalidOrderException when order type contains non-alphabetic characters
     */
    @Test
    public void OrderProducerService_ProduceOrder_NonAlphabeticType_ThrowsInvalidOrderException(){
        try {
            orderProducerService.produceOrder(OrderObject.NON_ALPHABETIC_TYPE_ORDER);
        }catch (InvalidOrderException e) {
            assertThat(e.getErrorMessage()).isEqualTo("Type of order is not valid");
        }
    }


}