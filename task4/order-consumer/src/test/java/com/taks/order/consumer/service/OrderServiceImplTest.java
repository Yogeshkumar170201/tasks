package com.taks.order.consumer.service;

import com.taks.order.consumer.dao.OrderDao;
import com.taks.order.consumer.exception.InvalidOrderException;
import com.taks.order.consumer.model.Orders;
import com.taks.order.consumer.utils.OrderDetailsObject;
import com.taks.order.consumer.utils.OrderObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import java.util.*;


/**
 * Unit testcases of Methods of OrderService
 */
@ExtendWith(MockitoExtension.class)
class OrderServiceImplTest {

    @Mock
    private OrderDao orderDao;

    @InjectMocks
    private OrderServiceImpl orderService;

    /**
     * Unit testcase of saveOrder of OrderService if order is valid
     */
//    @Test
//    public void OrderService_SaveOrder_ValidOrder_ReturnOrder() throws InvalidOrderException {
//        when(orderDao.save(ArgumentMatchers.any((Orders.class)))).thenReturn(OrderObject.VALID_ORDER);
//
//        Orders actual = orderService.saveOrder(OrderDetailsObject.VALID_ORDER);
//
//        verify(orderDao, times(1)).save(ArgumentMatchers.eq(OrderObject.VALID_ORDER));
//
//        assertThat(actual).isNotNull();
//        assertThat(actual).isEqualTo(OrderObject.VALID_ORDER);

//    }

    /**
     * Unit testcase of saveOrder of OrderService if order is invalid
     */
    @Test
    public void OrderService_SaveOrder_InvalidOrder_ThrowInvalidOrderException() {

        try {
            orderService.saveOrder(OrderDetailsObject.INVALID_ORDER);
        } catch (InvalidOrderException e) {
            List<String> exceptionMessages = List.of("Invalid Order", "Name of order is not valid", "Time of preparation should be positive", "Type of order is not valid");
            boolean isMatch = false;
            for (String exceptionMessage : exceptionMessages) {
                if (exceptionMessage.equals(e.getErrorMessage())) {
                    isMatch = true;
                    break;
                }
            }
            assertTrue(isMatch);
        }
    }


    /**
     * Unit testcase of getAllOrders of OrderService
     */
    @Test
    public void OrderService_GetAllOrders_ReturnAllOrders() {
        when(orderDao.findAll()).thenReturn(OrderObject.ORDERS_LIST);
        List<Orders> actual = orderService.getAllOrders();
        verify(orderDao, times(1)).findAll();
        assertEquals(actual, OrderObject.ORDERS_LIST);
    }

    /**
     * Unit testcase of updateStatusOfOrderByName of OrderService if name exist in database
     */
    @Test
    public void OrderService_UpdateStatusOfOrderByName_NameExist_ReturnUpdatedOrder() throws InvalidOrderException {
        when(orderDao.findByName(OrderObject.VALID_ORDER.getName())).thenReturn(OrderObject.VALID_ORDER);
        when(orderDao.save(any(Orders.class))).thenReturn(OrderObject.VALID_ORDER);
        Orders actual = orderService.updateStatusOfOrderByName(OrderObject.VALID_ORDER.getName());
        verify(orderDao, times(1)).findByName(OrderObject.VALID_ORDER.getName());
        verify(orderDao, times(1)).save(OrderObject.VALID_ORDER);
        assertEquals(actual, OrderObject.VALID_ORDER);
        assertEquals(actual.getStatus(), "Prepared");
    }

    /**
     * Unit testcase of updateStatusOfOrderByName of OrderService if name not exist in database
     */
    @Test
    public void OrderService_UpdateStatusOfOrderByName_NameNotExist_ReturnUpdatedOrder() {
        when(orderDao.findByName("Invalid Name")).thenReturn(null);
        try {
            orderService.updateStatusOfOrderByName("Invalid Name");
        } catch (InvalidOrderException e) {
            assertEquals(e.getErrorMessage(), "Order not found");
        }
        verify(orderDao, times(1)).findByName("Invalid Name");
    }


}