package com.taks.order.consumer.utils;

import com.taks.order.consumer.model.Orders;

import java.util.Date;
import java.util.List;

/**
 * Dummy objects of Orders for testing
 */
public class OrderObject {

    public static final Orders VALID_ORDER = Orders.builder()
            .name("Valid Order")
            .timeOfPreparation(10L)
            .type("Non Veg")
            .date(new Date())
            .status("In Preparation")
            .build();



    public static final Orders ORDER_1 = Orders.builder()
            .name("Valid Order One")
            .timeOfPreparation(10L)
            .type("Non Veg")
            .date(new Date())
            .status("In Preparation")
            .build();

    public static final Orders ORDER_2 = Orders.builder()
            .name("Valid Order Two")
            .timeOfPreparation(20L)
            .type("Veg")
            .date(new Date())
            .status("In Preparation")
            .build();

    public static final Orders ORDER_3 = Orders.builder()
            .name("Valid Order Three")
            .timeOfPreparation(30L)
            .type("Non Veg")
            .date(new Date())
            .status("In Preparation")
            .build();

    public static final Orders PREPARED_ORDER_1 = Orders.builder()
            .name("Valid Order One")
            .timeOfPreparation(10L)
            .type("Non Veg")
            .date(new Date())
            .status("Prepared")
            .build();

    public static final Orders PREPARED_ORDER_2 = Orders.builder()
            .name("Valid Order Two")
            .timeOfPreparation(20L)
            .type("Veg")
            .date(new Date())
            .status("Prepared")
            .build();

    public static final Orders PREPARED_ORDER_3 = Orders.builder()
            .name("Valid Order Three")
            .timeOfPreparation(30L)
            .type("Non Veg")
            .date(new Date())
            .status("Prepared")
            .build();


    public static final List<Orders> ORDERS_LIST = List.of(ORDER_1, ORDER_2, ORDER_3);

    public static final List<Orders> PREPARED_ORDERS_LIST = List.of(PREPARED_ORDER_1, PREPARED_ORDER_2, PREPARED_ORDER_3);


}
