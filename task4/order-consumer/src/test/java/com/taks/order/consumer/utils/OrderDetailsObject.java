package com.taks.order.consumer.utils;

import com.taks.order.consumer.dto.OrderDetails;

/**
 * Dummy Objects of OrderDetails for testing
 */
public class OrderDetailsObject {

    public static final OrderDetails VALID_ORDER = OrderDetails.builder()
            .name("Valid Order")
            .timeOfPreparation(10L)
            .type("Non Veg")
            .build();

    public static final OrderDetails INVALID_ORDER = OrderDetails.builder()
            .timeOfPreparation(10L)
            .type("Non Veg")
            .build();

}
