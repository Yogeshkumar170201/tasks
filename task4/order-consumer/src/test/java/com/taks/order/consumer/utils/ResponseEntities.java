package com.taks.order.consumer.utils;

import com.taks.order.consumer.model.Orders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Dummy objects of ResponseEntity for testing of Controller layer
 */
public class ResponseEntities {


    public static ResponseEntity<Map<String, String>> GET_ORDER_PREPARED_RESPONSE_VALID_NAME() {
        Map<String, String> MAP_ORDER_PREPARED_RESPONSE = new HashMap<>();
        MAP_ORDER_PREPARED_RESPONSE.put("message", "Order is prepared");
        MAP_ORDER_PREPARED_RESPONSE.put("httpStatus", String.valueOf(HttpStatus.OK));
        return ResponseEntity.ok(MAP_ORDER_PREPARED_RESPONSE);
    }

    public static ResponseEntity<Map<String, String>> GET_ORDER_PREPARED_RESPONSE_INVALID_NAME() {
        Map<String, String> MAP_ORDER_PREPARED_RESPONSE = new HashMap<>();
        MAP_ORDER_PREPARED_RESPONSE.put("message", "Order not found");
        MAP_ORDER_PREPARED_RESPONSE.put("httpStatus", String.valueOf(HttpStatus.BAD_REQUEST));
        return ResponseEntity.badRequest().body(MAP_ORDER_PREPARED_RESPONSE);
    }

    public static ResponseEntity<Map<String, String>> GET_ORDER_ORDER_CONSUMER_CONFIG_RESPONSE() {
        Map<String, String> res = new HashMap<>();
        res.put("message", "Order Added in Database Successfully");
        res.put("httpStatus", String.valueOf(HttpStatus.CREATED));
        return ResponseEntity.ok(res);
    }
}


