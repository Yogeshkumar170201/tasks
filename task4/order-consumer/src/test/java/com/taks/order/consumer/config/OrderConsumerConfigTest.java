package com.taks.order.consumer.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.taks.order.consumer.constants.AppConstants;
import com.taks.order.consumer.dto.OrderDetails;
import com.taks.order.consumer.exception.InvalidOrderException;
import com.taks.order.consumer.service.OrderService;
import com.taks.order.consumer.utils.JsonStrings;
import com.taks.order.consumer.utils.OrderDetailsObject;
import com.taks.order.consumer.utils.OrderObject;
import com.taks.order.consumer.utils.ResponseEntities;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderConsumerConfigTest {

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderConsumerConfig orderConsumerConfig;

    @Mock
    private ObjectMapper objectMapper;

//    @Test
//    public void OrderConsumerConfig_GetOrder_ValidOrder_ReturnResponseEntity() throws JsonProcessingException, InvalidOrderException {
//
//        when(objectMapper.readValue(ArgumentMatchers.any(String.class), OrderDetails.class)).thenReturn(OrderDetailsObject.VALID_ORDER);
//        when(orderService.saveOrder(ArgumentMatchers.any(OrderDetails.class))).thenReturn(OrderObject.VALID_ORDER);
//        ResponseEntity<Map<String, String>> actual  = orderConsumerConfig.getOrder(JsonStrings.VALUE_1);
//        verify(objectMapper, times(1)).readValue(JsonStrings.VALUE_1, OrderDetails.class);
//        verify(orderService, times(1)).saveOrder(ArgumentMatchers.eq(OrderObject.VALID_ORDER));
//        assertEquals(actual, ResponseEntities.GET_ORDER_ORDER_CONSUMER_CONFIG_RESPONSE());
//
//    }

}