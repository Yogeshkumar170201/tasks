package com.taks.order.consumer.webmodules;

import com.taks.order.consumer.exception.InvalidOrderException;
import com.taks.order.consumer.model.Orders;
import com.taks.order.consumer.service.OrderService;
import com.taks.order.consumer.utils.OrderObject;
import com.taks.order.consumer.utils.ResponseEntities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;


/**
 * OrderController class unit testcases
 */
@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    /**
     * unit testcase of  getAllOrders of Order Controller
     */
    @Test
    public void OrderController_GetAllOrders_ReturnListOfOrders(){
        when(orderService.getAllOrders()).thenReturn(OrderObject.ORDERS_LIST);
        List<Orders> orders = orderController.getAllOrders();
        verify(orderService, times(1)).getAllOrders();
        Assertions.assertEquals(orders, OrderObject.ORDERS_LIST);
    }

    /**
     * unit testcase of  updateOrderStatusByName of Order Controller if name is valid
     */
    @Test
    public void OrderController_UpdateOrderStatusByName_ValidName_ReturnResponseEntity() throws InvalidOrderException {
        when(orderService.updateStatusOfOrderByName(ArgumentMatchers.any(String.class))).thenReturn(OrderObject.VALID_ORDER);
        ResponseEntity<Map<String, String>> actual = orderController.updateOrderStatusByName(OrderObject.VALID_ORDER.getName());
        verify(orderService, times(1)).updateStatusOfOrderByName(OrderObject.VALID_ORDER.getName());
        Assertions.assertEquals(actual, ResponseEntities.GET_ORDER_PREPARED_RESPONSE_VALID_NAME());
    }

    /**
     * unit testcase of  updateOrderStatusByName of Order Controller if name is invalid
     */
    @Test
    public void OrderController_UpdateOrderStatusByName_InvalidName_ReturnResponseEntity() throws InvalidOrderException {
        when(orderService.updateStatusOfOrderByName(ArgumentMatchers.any(String.class))).thenThrow( new InvalidOrderException("Order not found", HttpStatus.BAD_REQUEST));
        ResponseEntity<Map<String, String>> actual = orderController.updateOrderStatusByName("Invalid Name");
        verify(orderService, times(1)).updateStatusOfOrderByName(ArgumentMatchers.eq("Invalid Name"));
        Assertions.assertEquals(actual, ResponseEntities.GET_ORDER_PREPARED_RESPONSE_INVALID_NAME());
    }

    /**
     * unit testcase of  getPreparedOrders of Order Controller if name is valid
     */
    @Test
    public void OrderController_GetPreparedOrders_ReturnsListOfOrders(){
        when(orderService.getPreparedOrders()).thenReturn(OrderObject.PREPARED_ORDERS_LIST);
        List<Orders> actual = orderController.getPreparedOrders();
        verify(orderService, times(1)).getPreparedOrders();
        Assertions.assertEquals(actual,OrderObject.PREPARED_ORDERS_LIST);
    }

    /**
     * unit testcase of  getInPreparationOrders of Order Controller if name is valid
     */
    @Test
    public void OrderController_GetInPreparationOrders_ReturnsListOfOrders(){
        when(orderService.getPreparedOrders()).thenReturn(OrderObject.ORDERS_LIST);
        List<Orders> actual = orderController.getPreparedOrders();
        verify(orderService, times(1)).getPreparedOrders();
        Assertions.assertEquals(actual,OrderObject.ORDERS_LIST);
    }

}