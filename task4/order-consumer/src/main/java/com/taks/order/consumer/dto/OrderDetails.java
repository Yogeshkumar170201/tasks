package com.taks.order.consumer.dto;

import lombok.*;

/**
 * DTO of Order
 */
@Data
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails {
        private String name;
        private Long timeOfPreparation;
        private String type;
}
